package ru.t1.ytarasov.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.response.AbstractResponse;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskCreateResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

}
