package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.model.IWBS;
import ru.t1.ytarasov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class TaskDTO extends AbstractProjectTaskModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(@NotNull String name) {
        super(name);
    }

    public TaskDTO(@NotNull String name, @NotNull String description) {
        super(name, description);
    }

    public TaskDTO(@NotNull String name, @NotNull Status status) {
        super(name, status);
    }

    public TaskDTO(@NotNull String name, @NotNull String description, @NotNull Status status) {
        super(name, description, status);
    }

    @Override
    public String toString() {
        return getId() + " " + name + ": " + description + ": " + Status.toName(status) + ": " + projectId;
    }

}
