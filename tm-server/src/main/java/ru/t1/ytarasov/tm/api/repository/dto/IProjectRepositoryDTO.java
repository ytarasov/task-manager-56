package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDTO> {

    @Nullable
    List<ProjectDTO> findAll(@NotNull final Comparator comparator);

    @Nullable
    List<ProjectDTO> findAll(@NotNull final String userId, @NotNull final Comparator comparator);

}
