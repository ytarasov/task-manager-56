package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.ytarasov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.repository.dto.ProjectRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public final class ProjectServiceDTO
        extends AbstractUserOwnedServiceDTO<ProjectDTO, ProjectRepositoryDTO> implements IProjectServiceDTO {

    @NotNull
    private IProjectRepositoryDTO getProjectRepositoryDTO() {
        return context.getBean(IProjectRepositoryDTO.class);
    }

    @Override
    @NotNull
    public ProjectDTO add(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models) throws Exception {
        for (ProjectDTO model : models) add(model);
        return models;
    }

    @NotNull
    @Override
    public ProjectDTO updateById(@Nullable final String userId,
                                 @Nullable final String id,
                                 @Nullable final String name,
                                 @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUpdated(new Date());
        return update(project);
    }

    @Override
    public void clear() throws Exception {
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public ProjectDTO changeProjectStatusById(@Nullable final String userId,
                                              @Nullable final String id,
                                              @Nullable final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null || status.getDisplayName().isEmpty()) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUpdated(new Date());
        return update(project);
    }

    @Override
    public @NotNull ProjectDTO update(@Nullable ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public @Nullable List<ProjectDTO> findAll() {
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId, comparator);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return findAll(sort.getComparator());
    }

    @Override
    public Long getSize() throws Exception {
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return findAll(userId, sort.getComparator());
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO remove(@Nullable final ProjectDTO project) throws Exception {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.remove(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    public ProjectDTO removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Nullable
    @Override
    public ProjectDTO removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

    @Nullable
    @Override
    public ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public @Nullable ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final ProjectDTO projectDTO = findOneById(userId, model.getId());
        if (projectDTO == null) throw new ProjectNotFoundException();
        return remove(projectDTO);
    }

    @Override
    public Boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Boolean existsById;
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsById = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

    @Override
    public Boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        Boolean existsById;
        @NotNull final IProjectRepositoryDTO repository = getProjectRepositoryDTO();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            existsById = repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
        return existsById;
    }

}
