package ru.t1.ytarasov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IAuthService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.dto.ISessionServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.exception.field.LoginEmptyException;
import ru.t1.ytarasov.tm.exception.field.PasswordEmptyException;
import ru.t1.ytarasov.tm.exception.user.AccessDeniedException;
import ru.t1.ytarasov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.util.CryptUtil;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.Date;

@Service
public final class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserServiceDTO userService;

    @NotNull
    private final ISessionServiceDTO sessionService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IPropertyService propertyService,
                       @NotNull final IUserServiceDTO userService,
                       @NotNull final ISessionServiceDTO sessionService) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new IncorrectLoginOrPasswordException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty()) throw new PasswordEmptyException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
        return getToken(user);
    }

    @NotNull
    @Override
    public UserDTO check(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new IncorrectLoginOrPasswordException();
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || hash.isEmpty()) throw new PasswordEmptyException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return user;
    }

    @Override
    @SneakyThrows
    public void logout(@Nullable final SessionDTO session) throws Exception {
        if (session == null) throw new AccessDeniedException();
        sessionService.remove(session);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    @SneakyThrows
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        return sessionService.add(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();
        return session;
    }

}
