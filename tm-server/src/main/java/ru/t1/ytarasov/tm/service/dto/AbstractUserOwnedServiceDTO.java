package ru.t1.ytarasov.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ytarasov.tm.repository.dto.AbstractUserOwnedRepositoryDTO;

@Service
public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO, R extends AbstractUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R> implements IUserOwnedServiceDTO<M> {
}
