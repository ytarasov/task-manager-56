package ru.t1.ytarasov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable
    List<Project> findAll(@Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final Sort sort) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final String userId, @Nullable final Comparator comparator) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception;

    @Nullable
    Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception;

    @NotNull
    Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception;

    @NotNull
    Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws Exception;

}
