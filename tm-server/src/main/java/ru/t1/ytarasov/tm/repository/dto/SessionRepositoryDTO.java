package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class SessionRepositoryDTO extends AbstractUserOwnedRepositoryDTO<SessionDTO> implements ISessionRepositoryDTO {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(s) FROM SessionDTO s", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM SessionDTO s WHERE s.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(s) FROM SessionDTO m WHERE s.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM SessionDTO s WHERE s.id = :id AND s.userId = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        return entityManager
                .createQuery("FROM SessionDTO", SessionDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT s FROM SessionDTO s WHERE s.id = :id", SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery("SELECT s FROM SessionDTO s WHERE s.id = :id AND p.userId = :userId", SessionDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstantDTO.TABLE_SESSION;
    }

}
