package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

public interface IProjectTaskServiceDTO {

    void bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws Exception;

    @NotNull
    ProjectDTO removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception;

    void clearAllProjects(@Nullable String userId) throws Exception;

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws Exception;

}
