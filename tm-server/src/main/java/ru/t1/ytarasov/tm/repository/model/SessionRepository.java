package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.ISessionRepository;
import ru.t1.ytarasov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(s) FROM Session s", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM Session s WHERE s.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) {
        @NotNull final String jpql = String.format("SELECT COUNT(m) FROM %s s WHERE s.user.id = :userId",
                getTableName());
        return entityManager
                .createQuery("SELECT COUNT(m) FROM Session s WHERE s.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM Session s WHERE s.id = :id AND s.user.id = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        return entityManager
                .createQuery("FROM Session", Session.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT s FROM Session s WHERE s.id = :id", Session.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT s FROM Session s WHERE s.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT s FROM %s s WHERE s.%s = :id AND s.%s = :userId",
                getTableName(), EntityConstant.COLUMN_ID, EntityConstant.COLUMN_USER_ID);
        return entityManager
                .createQuery("SELECT s FROM Session s WHERE s.id = :id AND s.user.id = :userId", Session.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstant.TABLE_SESSION;
    }

}
