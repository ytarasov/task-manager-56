package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.dto.IServiceDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;
import ru.t1.ytarasov.tm.repository.dto.AbstractRepositoryDTO;

@Service
public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends AbstractRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
