package ru.t1.ytarasov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IPropertyService;

import java.util.Properties;

@Service
public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356585958";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_KEY_DEFAULT = "123";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "1010";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:mysql://localhost:3306/tm";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USERNAME_DEFAULT = "sa";

    @NotNull
    private static final String DATABASE_PASSWORD_DEFAULT = "";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_DRIVER_DEFAULT = "com.mysql.cj.jdbc.Driver";

    @NotNull
    private static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT_KEY = "database.dialect";

    @NotNull
    private static final String DATABASE_DIALECT_DEFAULT = "org.hibernate.dialect.MySQLInnoDBDialect";

    @NotNull
    private static final String DATABASE_HBM2DDLAUTO_KEY = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_HBM2DDLAUTO_DEFAULT = "update";

    @NotNull
    private static final String DATABASE_SHOW_SQL_KEY = "database.show_sql";

    @NotNull
    private static final String DATABASE_SHOW_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_KEY = "database.format_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CASH_KEY = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CASH_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS_KEY = "database.factory_class";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS_DEFAULT =
            "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private static final String DATABASE_USE_QUERY_CASH_KEY = "database.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CASH_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS_KEY = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS_DEFAULT = "true";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_KEY = "database.region_prefix";

    @NotNull
    private static final String DATABASE_REGION_PREFIX_DEFAULT = "task-manager";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH_KEY = "database.config_file_path";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH_DEFAULT = "hazelcast.xml";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        @NotNull final String passwordSecret = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(passwordSecret);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    public @NotNull Integer getServerPort() {
        @NotNull final String serverPort = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(serverPort);
    }

    @Override
    public @NotNull String getSessionKey() {
        return getStringValue(SESSION_KEY, SESSION_KEY_DEFAULT);
    }

    @Override
    public @NotNull Integer getSessionTimeout() {
        @NotNull final String sessionTimeout = getStringValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
        return Integer.parseInt(sessionTimeout);
    }

    @Override
    public @NotNull String getDBUrl() {
        @NotNull final String dbUrl = getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
        return dbUrl;
    }

    @Override
    public @NotNull String getDBUsername() {
        @NotNull final String dbUsername = getStringValue(DATABASE_USERNAME_KEY, DATABASE_USERNAME_DEFAULT);
        return dbUsername;
    }

    @Override
    public @NotNull String getDBPassword() {
        @NotNull final String dbPassword = getStringValue(DATABASE_PASSWORD_KEY, DATABASE_PASSWORD_DEFAULT);
        return dbPassword;
    }

    @Override
    public @NotNull String getDBDriver() {
        @NotNull final String dbDriver = getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
        return dbDriver;
    }

    @Override
    public @NotNull String getDBDialect() {
        return getStringValue(DATABASE_DIALECT_KEY, DATABASE_DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDLAUTO_KEY, DATABASE_HBM2DDLAUTO_DEFAULT);
    }

    @Override
    public @NotNull String getShowSql() {
        return getStringValue(DATABASE_SHOW_SQL_KEY, DATABASE_SHOW_SQL_DEFAULT);
    }

    @Override
    public @NotNull String getFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL_KEY, DATABASE_FORMAT_SQL_DEFAULT);
    }

    @NotNull
    @Override
    public String getSecondLvlCash() {
        return getStringValue(DATABASE_SECOND_LVL_CASH_KEY, DATABASE_SECOND_LVL_CASH_DEFAULT);
    }

    @NotNull
    @Override
    public String getFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS_KEY, DATABASE_FACTORY_CLASS_DEFAULT);
    }

    @NotNull
    @Override
    public String getUseQueryCash() {
        return getStringValue(DATABASE_USE_QUERY_CASH_KEY, DATABASE_USE_QUERY_CASH_DEFAULT);
    }
    @NotNull
    @Override
    public String getUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS_KEY, DATABASE_USE_MIN_PUTS_DEFAULT);
    }
    @NotNull
    @Override
    public String getRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX_KEY, DATABASE_REGION_PREFIX_DEFAULT);
    }

    @NotNull
    @Override
    public String getConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH_KEY, DATABASE_CONFIG_FILE_PATH_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace('.', '_').toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getPort() {
        return getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

}
