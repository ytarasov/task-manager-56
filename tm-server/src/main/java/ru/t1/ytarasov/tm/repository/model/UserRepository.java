package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.IUserRepository;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository
        extends AbstractRepository<User> implements IUserRepository {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(u) FROM User u", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM User u WHERE u.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = String.format("SELECT u FROM %s u", getTableName());
        return entityManager
                .createQuery("FROM User", User.class)
                //.setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        @NotNull final String jpql = String.format("SELECT u FROM %s u WHERE u.%s = :id",
                getTableName(), EntityConstant.COLUMN_ID);
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.id = :id", User.class)
                .setParameter("id", id)
                .getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.login = :login", User.class)
                .setParameter("login", login)
                .getResultStream().findFirst().orElse(null);
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT u FROM User u WHERE u.email = :email", User.class)
                .setParameter("email", email)
                .getResultStream().findFirst().orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final User user = this.findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    public Boolean isLoginExists(@NotNull String login) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) = 1 FROM %s u WHERE u.%s = :login",
                getTableName(), EntityConstant.COLUMN_LOGIN);
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM User u WHERE u.login = :login", Boolean.class)
                .setParameter(EntityConstant.COLUMN_LOGIN, login)
                .getSingleResult();
    }

    @Override
    public Boolean isEmailExists(@NotNull String email) {
        @NotNull final String jpql = String.format("SELECT COUNT(1) = 1 FROM %s u WHERE u.%s = :email",
                getTableName(), EntityConstant.COLUMN_EMAIL);
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM User u WHERE u.email = :email", Boolean.class)
                .setParameter(EntityConstant.COLUMN_EMAIL, email)
                .getSingleResult();
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstant.TABLE_USER;
    }

}
