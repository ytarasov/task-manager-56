package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.api.service.model.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.TaskIdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    public void bindTaskToProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    public void removeProject(@Nullable String userId, @Nullable Project project) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final List<Task> tasks = taskService.findByProjectId(userId, project.getId());
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        for (@NotNull Task task : tasks) taskService.remove(task);
    }

    @Override
    public void removeProjectById(@Nullable String userId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        removeProject(userId, project);
    }

    @Override
    public void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId, @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(null);
        taskService.update(task);
    }

}
