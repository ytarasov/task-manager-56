package ru.t1.ytarasov.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepositoryDTO
        extends AbstractUserOwnedRepositoryDTO<ProjectDTO> implements IProjectRepositoryDTO {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM ProjectDTO p", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM ProjectDTO m WHERE m.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM ProjectDTO p WHERE p.id = :id AND p.userId = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@NotNull Comparator comparator) {
        return entityManager
                .createQuery("SELECT p FROM ProjectDTO m ORDER BY :sort", ProjectDTO.class)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Override
    public @Nullable List<ProjectDTO> findAll(@NotNull String userId, @NotNull Comparator comparator) {
        return entityManager
                .createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = userId ORDER BY :sort", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager
                .createQuery("FROM ProjectDTO", ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT p FROM ProjectDTO p WHERE p.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull String userId) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = String.format("SELECT p FROM %s p WHERE p.%s = :id AND p.%s = :userId",
                getTableName(), EntityConstantDTO.COLUMN_ID, EntityConstantDTO.COLUMN_USER_ID);
        return entityManager
                .createQuery("SELECT p FROM ProjectDTO p WHERE p.id = :id AND p.userId = :userId", ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    protected @NotNull String getTableName() {
        return EntityConstantDTO.TABLE_PROJECT;
    }

}
