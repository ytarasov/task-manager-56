package ru.t1.ytarasov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstant;
import ru.t1.ytarasov.tm.api.repository.model.ITaskRepository;
import ru.t1.ytarasov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskRepository
        extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Long getSize() {
        return entityManager
                .createQuery("SELECT COUNT(t) FROM Task t", Long.class)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM Task t WHERE t.id = :id", Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Long getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(t) FROM Task t WHERE t.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT COUNT(1) = 1 FROM Task t WHERE t.id = :id AND t.user.id = :userId", Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Nullable
    @Override
    public List<Task> findAll() {
        return entityManager
                .createQuery("FROM Task", Task.class)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String id) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.id = :id", Task.class)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final Comparator comparator) {
        return entityManager
                .createQuery("SELECT t FROM Task t ORDER BY :sort", Task.class)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator comparator) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = userId ORDER BY :sort", Task.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedType(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        return entityManager.
                createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("userId", userId).
                getResultList();

    }

    @Nullable
    @Override
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return EntityConstant.TABLE_TASK;
    }

}
