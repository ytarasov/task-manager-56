package ru.t1.ytarasov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.endpoint.*;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.api.service.dto.*;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.api.service.model.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.endpoint.*;
import ru.t1.ytarasov.tm.service.*;
import ru.t1.ytarasov.tm.service.dto.*;
import ru.t1.ytarasov.tm.service.model.ProjectService;
import ru.t1.ytarasov.tm.service.model.ProjectTaskService;
import ru.t1.ytarasov.tm.service.model.TaskService;
import ru.t1.ytarasov.tm.service.model.UserService;
import ru.t1.ytarasov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IConnectionService connectionService;

    @NotNull
    @Autowired
    private IProjectServiceDTO projectServiceDTO;

    @Autowired
    private ITaskServiceDTO taskServiceDTO;

    @NotNull
    @Autowired
    private IProjectTaskServiceDTO projectTaskServiceDTO;

    @NotNull
    @Autowired
    private ISessionServiceDTO sessionServiceDTO;

    @NotNull
    @Autowired
    private IUserServiceDTO userServiceDTO;

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @SneakyThrows
    private void initLogger() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
    }

    @SneakyThrows
    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getHost();
        @NotNull final String port = propertyService.getPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void prepareStart() {
        initPid();
        //initLogger();
        connectionService.initLogger();
        initEndpoints();
        loggerService.info("** WELCOME TO THE TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    public void run() {
        prepareStart();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
