package ru.t1.ytarasov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends IRepositoryDTO<M> {

    void clear(@NotNull final String userId);

    @Nullable
    List<M> findAll(@NotNull final String userId);

    @Nullable
    M findOneById(@NotNull final String userId, @NotNull final String id);

    Long getSize(@NotNull final String id);

    Boolean existsById(@NotNull final String userId, @NotNull final String id);

}
