package ru.t1.ytarasov.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO>
        extends AbstractRepositoryDTO<M> implements IUserOwnedRepositoryDTO<M> {

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null || models.isEmpty()) return;
        for (@NotNull final M model : models) remove(model);
    }

}
