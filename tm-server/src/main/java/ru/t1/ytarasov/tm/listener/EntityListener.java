package ru.t1.ytarasov.tm.listener;

import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.log.JmsLoggerProducer;
import ru.t1.ytarasov.tm.log.OperationEvent;
import ru.t1.ytarasov.tm.log.OperationType;

public class EntityListener implements PostInsertEventListener, PostUpdateEventListener, PostDeleteEventListener {

    @NotNull
    private final JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(PostDeleteEvent postDeleteEvent) {
        log(OperationType.DELETE, postDeleteEvent.getEntity());
    }

    @Override
    public void onPostInsert(PostInsertEvent postInsertEvent) {
        log(OperationType.INSERT, postInsertEvent.getEntity());
    }

    @Override
    public void onPostUpdate(PostUpdateEvent postUpdateEvent) {
        log(OperationType.UPDATE, postUpdateEvent.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(EntityPersister entityPersister) {
        return false;
    }

    public void log(@NotNull final OperationType operationType, @NotNull final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(operationType, entity));
    }

}
