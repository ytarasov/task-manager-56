package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.configuration.ServerConfiguration;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.field.EmailEmptyException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.LoginEmptyException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private static final String TEST_LOGIN = "TEST";

    @NotNull
    private static final String TEST_PASS = "TEST";

    @NotNull
    private static final String TEST_EMAIL = "TEST@tst.ru";

    @NotNull
    private static final String NEW_USER_LOGIN = "TEST_ADD";

    @NotNull
    private static final String NEW_USER_PASS = "TEST_ADD";

    @NotNull
    private static final String NEW_USER_EMAIL = "TEST_ADD@tst.ru";

    @NotNull
    private static final String FIND_BY_ID_USER_LOGIN = "TEST_FIND_BY_ID";

    @NotNull
    private static final String FIND_BY_ID_USER_PASS = "TEST_FIND_BY_ID";

    @NotNull
    private static final String FIND_BY_ID_USER_EMAIL = "TEST_FIND_BY_ID@tst.ru";

    @NotNull
    private static final String REMOVE_USER_LOGIN = "TEST_REMOVE";

    @NotNull
    private static final String REMOVE_USER_PASS = "TEST_REMOVE";

    @NotNull
    private static final String REMOVE_USER_EMAIL = "TEST_REMOVE@tst.ru";

    @NotNull
    private static final String REMOVE_BY_ID_USER_LOGIN = "TEST_REMOVE_BY_ID";

    @NotNull
    private static final String REMOVE_BY_ID_USER_PASS = "TEST_REMOVE_BY_ID";

    @NotNull
    private static final String REMOVE_BY_ID_USER_EMAIL = "TEST_REMOVE_BY_ID@tst.ru";

    @NotNull
    private static final String REMOVE_BY_LOGIN_USER_LOGIN = "TEST_REMOVE_BY_LOGIN";

    @NotNull
    private static final String REMOVE_BY_LOGIN_USER_PASS = "TEST_REMOVE_BY_LOGIN";

    @NotNull
    private static final String REMOVE_BY_LOGIN_USER_EMAIL = "TEST_REMOVE_BY_LOGIN@tst.ru";

    @NotNull
    private static final String TEST_LOCK_USER_LOGIN = "TEST_LOCK_USER";

    @NotNull
    private static final String TEST_LOCK_USER_PASSWORD = "TEST_LOCK_USER";

    @NotNull
    private static final String TEST_LOCK_USER_EMAIL = "TEST_LOCK_USER@tst.ru";

    @NotNull
    private static final String TEST_UNLOCK_USER_LOGIN = "TEST_UNLOCK_USER";

    @NotNull
    private static final String TEST_UNLOCK_USER_PASSWORD = "TEST_UNLOCK_USER";

    @NotNull
    private static final String TEST_UNLOCK_USER_EMAIL = "TEST_UNLOCK_USER@tst.ru";

    @NotNull
    @Autowired
    private static IPropertyService propertyService;

    @NotNull
    private static IUserService userService;

    @Nullable
    private static final User userNull = null;

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserService.class);
        userService.create(TEST_LOGIN, TEST_PASS, TEST_EMAIL, Role.USUAL);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        userService.clear();
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        final int listSize = users.size();
        final int fondSize = userService.getSize().intValue();
        Assert.assertEquals(listSize, fondSize);
    }

    @Test
    public void findAll() throws Exception {
        @Nullable final List<User> users = userService.findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() >= 0);
    }

    @Test
    public void add() throws Exception {
        final int expectedSize = userService.getSize().intValue() + 1;
        Assert.assertThrows(UserNotFoundException.class, () -> userService.add(userNull));
        @NotNull final User user = new User();
        user.setLogin(NEW_USER_LOGIN);
        @Nullable final String passwordHash = HashUtil.salt(NEW_USER_PASS, "452316", 7657);
        Assert.assertNotNull(passwordHash);
        user.setPasswordHash(passwordHash);
        user.setEmail(NEW_USER_EMAIL);
        user.setRole(Role.USUAL);
        userService.add(user);
        final int foundSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void findById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
        @NotNull final User user = userService.create(FIND_BY_ID_USER_LOGIN, FIND_BY_ID_USER_PASS, FIND_BY_ID_USER_EMAIL, Role.USUAL);
        @Nullable final User foundUser = userService.findOneById(user.getId());
        Assert.assertNotNull(foundUser);
        Assert.assertEquals(user.getEmail(), foundUser.getEmail());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        @Nullable final User user = userService.findByLogin(TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getEmail(), TEST_EMAIL);
    }

    @Test
    public void findByEmail() throws Exception {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(""));
        @Nullable final User user = userService.findByEmail(TEST_EMAIL);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getLogin(), TEST_LOGIN);
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.remove(null));
        @Nullable final User user = userService.create(REMOVE_USER_LOGIN, REMOVE_USER_PASS, REMOVE_USER_EMAIL, Role.USUAL);
        Assert.assertNotNull(user);
        final int expectedSize = userService.getSize().intValue() - 1;
        userService.remove(user);
        final int foundSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        @Nullable final User user = userService.create(REMOVE_BY_ID_USER_LOGIN, REMOVE_BY_ID_USER_PASS, REMOVE_BY_ID_USER_EMAIL, Role.USUAL);
        Assert.assertNotNull(user);
        final int expectedSize = userService.getSize().intValue() - 1;
        userService.removeById(user.getId());
        final int foundSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void removeByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        userService.create(REMOVE_BY_LOGIN_USER_LOGIN, REMOVE_BY_LOGIN_USER_PASS, REMOVE_BY_LOGIN_USER_EMAIL, Role.USUAL);
        final int expectedSize = userService.getSize().intValue() - 1;
        userService.removeByLogin(REMOVE_BY_LOGIN_USER_LOGIN);
        final int foundSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void lockUser() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUser(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUser(""));
        userService.create(TEST_LOCK_USER_LOGIN, TEST_LOCK_USER_PASSWORD, TEST_LOCK_USER_EMAIL, Role.USUAL);
        userService.lockUser(TEST_LOCK_USER_LOGIN);
        @Nullable final User foundUser = userService.findByLogin(TEST_LOCK_USER_LOGIN);
        Assert.assertNotNull(foundUser);
        Assert.assertTrue(foundUser.isLocked());
    }

    @Test
    public void unlockUser() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUser(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUser(""));
        userService.create(TEST_UNLOCK_USER_LOGIN, TEST_UNLOCK_USER_PASSWORD, TEST_UNLOCK_USER_EMAIL, Role.USUAL);
        userService.unlockUser(TEST_UNLOCK_USER_LOGIN);
        @Nullable final User foundUser = userService.findByLogin(TEST_UNLOCK_USER_LOGIN);
        Assert.assertNotNull(foundUser);
        Assert.assertFalse(foundUser.isLocked());
    }

    @Test
    public void clear() throws Exception {
        userService.clear();
        Assert.assertEquals(0, userService.getSize().intValue());
        userService.create(TEST_LOGIN, TEST_PASS, TEST_EMAIL, Role.USUAL);
    }

}
