package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.configuration.ServerConfiguration;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.exception.user.ExistsEmailException;
import ru.t1.ytarasov.tm.exception.user.ExistsLoginException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.service.dto.UserServiceDTO;
import ru.t1.ytarasov.tm.util.HashUtil;

import java.util.List;

@Category(UnitCategory.class)
public class UserDTOServiceTest {

    @NotNull
    private static final String TEST_LOGIN = "TEST";

    @NotNull
    private static final String TEST_PASS = "TEST";

    @NotNull
    private static final String TEST_EMAIL = "TEST@tst.ru";

    @NotNull
    private static final String NEW_USER_LOGIN = "TEST_ADD";

    @NotNull
    private static final String NEW_USER_PASS = "TEST_ADD";

    @NotNull
    private static final String NEW_USER_EMAIL = "TEST_ADD@tst.ru";

    @NotNull
    private static final String FIND_BY_ID_USER_LOGIN = "TEST_FIND_BY_ID";

    @NotNull
    private static final String FIND_BY_ID_USER_PASS = "TEST_FIND_BY_ID";

    @NotNull
    private static final String FIND_BY_ID_USER_EMAIL = "TEST_FIND_BY_ID@tst.ru";

    @NotNull
    @Autowired
    private static IPropertyService propertyService = new PropertyService();

    @NotNull
    private static IUserServiceDTO userService;

    @BeforeClass
    public static void setup() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserServiceDTO.class);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, TEST_PASS);
        Assert.assertNotNull(passwordHash);
        userService.add(new UserDTO(TEST_LOGIN, passwordHash, TEST_EMAIL));
    }

    @AfterClass
    public static void tearDown() throws Exception {
        userService.clear();
    }

    @Test
    public void add() throws Exception {
        final int firstSize = userService.getSize().intValue();
        userService.add(new UserDTO(NEW_USER_LOGIN, HashUtil.salt(propertyService, NEW_USER_PASS), NEW_USER_EMAIL));
        final int expectedSize = firstSize + 1;
        final int currentSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void existsById() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin(TEST_LOGIN);
        Assert.assertNotNull(user);
        @NotNull final String id = user.getId();
        Assert.assertTrue(userService.existsById(id));
    }

    @Test
    public void findAll() throws Exception {
        Assert.assertEquals(userService.getSize().intValue(), userService.findAll().size());
    }

    @Test
    public void getSize() throws Exception {
        final int size1 = userService.findAll().size();
        final int size2 = userService.getSize().intValue();
        Assert.assertEquals(size1, size2);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.findOneById(""));
        @NotNull final UserDTO user = userService.create(FIND_BY_ID_USER_LOGIN, FIND_BY_ID_USER_PASS, FIND_BY_ID_USER_EMAIL, Role.USUAL);
        @NotNull final String id = user.getId();
        @Nullable final UserDTO user1 = userService.findOneById(id);
        Assert.assertNotNull(user1);
        Assert.assertEquals(id, user1.getId());
    }

    @Test
    public void findByLogin() throws Exception {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        @Nullable final UserDTO user = userService.findByLogin(TEST_LOGIN);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_EMAIL, user.getEmail());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(UserNotFoundException.class, () -> userService.remove(null));
        @NotNull final UserDTO user = userService.add(new UserDTO("trt", HashUtil.salt(propertyService, "trt"), "trt@trt.tu"));
        final int expectedSize = userService.getSize().intValue() - 1;
        userService.remove(user);
        final int currentSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(null));
        @NotNull final UserDTO user = userService.add(new UserDTO("trt1", HashUtil.salt(propertyService, "trt1"), "trt1@trt.ru"));
        final int expectedSize = userService.getSize().intValue() - 1;
        userService.removeById(user.getId());
        final int currentSize = userService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void clear() throws Exception {
        @NotNull final List<UserDTO> users = userService.findAll();
        userService.clear();
        Assert.assertEquals(0, userService.getSize().intValue());
        for (UserDTO user : users) userService.add(user);
    }

}
