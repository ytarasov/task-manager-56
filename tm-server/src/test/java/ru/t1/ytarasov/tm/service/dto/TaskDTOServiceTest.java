package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.configuration.ServerConfiguration;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ModelNotFoundException;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.service.dto.ProjectServiceDTO;
import ru.t1.ytarasov.tm.service.dto.ProjectTaskServiceDTO;
import ru.t1.ytarasov.tm.service.dto.TaskServiceDTO;
import ru.t1.ytarasov.tm.service.dto.UserServiceDTO;

import java.util.List;

@Category(UnitCategory.class)
public class TaskDTOServiceTest {

    @NotNull
    private static final String NEW_TASK_NAME = "new task";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "new task";

    @NotNull
    private static final String UPDATE_TASK_NAME = "update task";

    @NotNull
    private static final String UPDATE_TASK_DESCRIPTION = "update task";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_TASK_NAME = "remove";

    @NotNull
    @Autowired
    private static IPropertyService propertyService;

    @NotNull
    private static ITaskServiceDTO taskService;

    @NotNull
    private static IProjectServiceDTO projectService;

    @NotNull
    private static IProjectTaskServiceDTO projectTaskService;

    @NotNull
    private static IUserServiceDTO userService;

    private static String userId = "";

    private static String projectId = "";

    @BeforeClass
    public static void createUser() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserServiceDTO.class);
        projectService = context.getBean(IProjectServiceDTO.class);
        taskService = context.getBean(ITaskServiceDTO.class);
        projectTaskService = context.getBean(IProjectTaskServiceDTO.class);
        @NotNull final UserDTO user = userService.create("test26", "test2", "test2@test", Role.USUAL);
        userId = user.getId();
        @NotNull final ProjectDTO project = projectService.create(userId, "test project", "test project");
        projectId = project.getId();
        @NotNull final TaskDTO task = taskService.create(userId, "test task", "test task");
    }

    @AfterClass
    public static void removeUser() throws Exception {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void findAll() throws Exception {
        final int expectedSize = taskService.getSize().intValue();
        @NotNull final List<TaskDTO> tasks = taskService.findAll();
        final int currentSize = tasks.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(""));
        @NotNull final TaskDTO task = new TaskDTO(NEW_TASK_NAME);
        task.setUserId(userId);
        taskService.add(task);
        @Nullable final TaskDTO task1 = taskService.findOneById(task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int currentSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final TaskDTO task = new TaskDTO(CHANGE_STATUS_NAME);
        task.setUserId(userId);
        taskService.add(task);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById("", task.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById(userId, task.getId(), null));
        @Nullable final TaskDTO task1 = taskService.changeTaskStatusById(userId, task.getId(), Status.COMPLETED);
        Assert.assertNotNull(task1);
        Assert.assertEquals(task.getId(), task1.getId());
        Assert.assertEquals(task1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws Exception {
        @NotNull final TaskDTO task = taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById("", task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, null, UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById(userId, "", UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), null, UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById(userId, task.getId(), "", UPDATE_TASK_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, ""));
        @NotNull final TaskDTO task1 = taskService.updateById(userId, task.getId(), UPDATE_TASK_NAME, UPDATE_TASK_DESCRIPTION);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(null));
        @NotNull final TaskDTO task = new TaskDTO(REMOVE_TASK_NAME);
        task.setUserId(userId);
        taskService.add(task);
        taskService.remove(task);
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.removeById(""));
        @NotNull final TaskDTO task = new TaskDTO(REMOVE_TASK_NAME);
        task.setUserId(userId);
        taskService.add(task);
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findOneById(task.getId()));
    }

    @Test
    public void getSizeWithUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.getSize(null));
        final int sizeExpected = taskService.getSize(userId).intValue() + 1;
        taskService.create(userId, NEW_TASK_NAME, NEW_TASK_DESCRIPTION);
        final int sizeCurrent = taskService.getSize(userId).intValue();
        Assert.assertEquals(sizeExpected, sizeCurrent);
    }

    @Test
    public void bindTaskToProject() throws Exception {
        @Nullable final TaskDTO task = taskService.create(userId, "test bind to project", NEW_TASK_DESCRIPTION);
        Assert.assertNotNull(task);
        projectTaskService.bindTaskToProject(userId, task.getId(), projectId);
        @Nullable final TaskDTO task1 = taskService.findOneById(userId, task.getId());
        Assert.assertNotNull(task1);
        Assert.assertEquals(projectId, task1.getProjectId());
    }

    @Test
    public void clear() throws Exception {
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize().intValue());
    }

}
