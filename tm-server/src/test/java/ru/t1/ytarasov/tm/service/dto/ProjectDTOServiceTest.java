package ru.t1.ytarasov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.ytarasov.tm.configuration.ServerConfiguration;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;

import java.util.List;

@Category(UnitCategory.class)
public class ProjectDTOServiceTest {

    @NotNull
    private static final String NEW_PROJECT_NAME = "new project";

    @NotNull
    private static final String NEW_PROJECT_DESCRIPTION = "new project";

    @NotNull
    private static final String UPDATE_PROJECT_NAME = "update project";

    @NotNull
    private static final String UPDATE_PROJECT_DESCRIPTION = "update project";

    @NotNull
    private static final String CHANGE_STATUS_NAME = "test change status";

    @NotNull
    private static final String REMOVE_PROJECT_NAME = "remove";

    @NotNull
    @Autowired
    private static IPropertyService propertyService;

    @NotNull
    private static IProjectServiceDTO projectService;

    @NotNull
    private static IUserServiceDTO userService;

    @NotNull
    private static String userId = "";

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserServiceDTO.class);
        projectService = context.getBean(IProjectServiceDTO.class);
        @NotNull final UserDTO user = userService.create("test1", "test1", "test11@test", Role.USUAL);
        userId = user.getId();
        projectService.create(userId, "test project", "test project");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        projectService.clear();
        userService.clear();
    }

    @Test
    public void findAll() throws Exception {
        final int expectedSize = projectService.getSize().intValue();
        @Nullable final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        final int currentSize = projects.size();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(""));
        @NotNull final ProjectDTO project = new ProjectDTO(NEW_PROJECT_NAME);
        project.setUserId(userId);
        projectService.add(project);
        @Nullable final ProjectDTO project1 = projectService.findOneById(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        final int expectedSize = projects.size();
        final int currentSize = projectService.getSize().intValue();
        Assert.assertEquals(expectedSize, currentSize);
    }

    @Test
    public void changeStatusById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(userId, "", Status.IN_PROGRESS));
        @NotNull final ProjectDTO project = new ProjectDTO(CHANGE_STATUS_NAME);
        project.setUserId(userId);
        projectService.add(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(null, project.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById("", project.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusById(userId, project.getId(), null));
        @Nullable final ProjectDTO project1 = projectService.changeProjectStatusById(userId, project.getId(), Status.COMPLETED);
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals(project1.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateById() throws Exception {
        @NotNull final ProjectDTO project = projectService.create(userId, NEW_PROJECT_NAME, NEW_PROJECT_DESCRIPTION);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(null, project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById("", project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, null, UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(userId, "", UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, project.getId(), null, UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(userId, project.getId(), "", UPDATE_PROJECT_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, ""));
        @NotNull final ProjectDTO project1 = projectService.updateById(userId, project.getId(), UPDATE_PROJECT_NAME, UPDATE_PROJECT_DESCRIPTION);
        Assert.assertEquals(project.getId(), project1.getId());
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.remove(null));
        @NotNull final ProjectDTO project = new ProjectDTO(REMOVE_PROJECT_NAME);
        project.setUserId(userId);
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void removeById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(""));
        @NotNull final ProjectDTO project = new ProjectDTO(REMOVE_PROJECT_NAME);
        project.setUserId(userId);
        projectService.add(project);
        projectService.removeById(project.getId());
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    public void clear() throws Exception {
        projectService.clear();
        Assert.assertEquals(0, projectService.getSize().intValue());

    }

}
