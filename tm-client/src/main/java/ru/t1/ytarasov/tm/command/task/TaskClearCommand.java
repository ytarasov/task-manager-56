package ru.t1.ytarasov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.task.TaskClearRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear all tasks";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASK LIST]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        getTaskEndpoint().clearTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
