package ru.t1.ytarasov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.project.ProjectClearRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear all projects";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR PROJECT LIST]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        getProjectEndpoint().clearProjects(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
