package ru.t1.ytarasov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.ytarasov.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.ytarasov.tm.exception.AbstractException;

@Component
public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show information about application developer";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getAbout(request);
        @Nullable final String authorName = response.getName();
        @Nullable final String email = response.getEmail();
        System.out.println("Author: " + authorName);
        System.out.println("E-mail: " + email);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
