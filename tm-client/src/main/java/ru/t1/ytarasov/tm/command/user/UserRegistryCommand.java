package ru.t1.ytarasov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.user.UserRegistryRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "registry";

    @NotNull
    public static final String DESCRIPTION = "Registry user";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("ENTER ROLE");
        System.out.println(Arrays.toString(Role.values()));
        @Nullable final String roleValue = TerminalUtil.nextLine();
        @Nullable final Role role = Role.toRole(roleValue);
        @Nullable final String token = getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(token);
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        request.setRole(role);
        getUserEndpoint().registryUser(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
