package ru.t1.ytarasov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(getToken());
        request.setTaskId(id);
        getTaskEndpoint().removeTaskById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
