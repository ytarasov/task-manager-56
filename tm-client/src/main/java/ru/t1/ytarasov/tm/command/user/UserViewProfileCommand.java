package ru.t1.ytarasov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

@Component
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "view-profile";

    @NotNull
    public static final String DESCRIPTION = "View user profile";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[VIEW USER PROFILE]");
        @Nullable final String token = getToken();
        @NotNull final UserProfileRequest request = new UserProfileRequest(token);
        @Nullable final UserDTO user = getAuthEndpoint().profile(request).getUser();
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("login: " + user.getLogin());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
