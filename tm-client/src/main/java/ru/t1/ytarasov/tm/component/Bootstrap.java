package ru.t1.ytarasov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.repository.ICommandRepository;
import ru.t1.ytarasov.tm.api.service.ICommandService;
import ru.t1.ytarasov.tm.api.service.ILoggerService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.ITokenService;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.exception.system.CommandNotSupportedException;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.service.CommandService;
import ru.t1.ytarasov.tm.service.LoggerService;
import ru.t1.ytarasov.tm.service.PropertyService;
import ru.t1.ytarasov.tm.service.TokenService;
import ru.t1.ytarasov.tm.util.SystemUtil;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_NAME = "ru.t1.ytarasov.tm.command";

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    private void prepareStart() {
        initPid();
        initCommands(abstractCommands);
        initLogger();
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void run(@Nullable String[] args) {
        if (runArguments(args)) System.exit(0);
        prepareStart();
        System.out.println();
        runCommands();
    }

    private boolean runArguments(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        try {
            runArgument(arg);
        } catch (@Nullable final Exception e) {
            loggerService.error(e);
            System.out.println();
        }
        return true;
    }

    @SneakyThrows
    private void runArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void runCommands() {
        while (!Thread.currentThread().isInterrupted()) runCommand();
    }

    private void runCommand() {
        try {
            @Nullable String command = TerminalUtil.nextLine();
            runCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@Nullable final Exception e) {
            loggerService.error(e);
            System.out.println();
            System.out.println("[FAIL]");
        }
    }

    @SneakyThrows
    public void runCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    @SneakyThrows
    public void initCommands(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

}
