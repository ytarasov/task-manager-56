package ru.t1.ytarasov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.model.ICommand;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.util.Collection;

@Component
public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "command";

    @Nullable
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show command list";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMMANDS]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        if (commands == null) return;
        for (@NotNull final ICommand command : commands) {
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
