package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ytarasov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ytarasov.tm.dto.request.user.UserProfileRequest;
import ru.t1.ytarasov.tm.dto.response.user.UserLoginResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserLogoutResponse;
import ru.t1.ytarasov.tm.dto.response.user.UserProfileResponse;
import ru.t1.ytarasov.tm.marker.SoapCategory;

import java.util.UUID;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint endpoint = IAuthEndpoint.newInstance();

    @NotNull
    private static final String ADMIN_LOGIN_PASSWORD = "ADMIN";

    @NotNull
    private static final String NEW_USER_LOGIN_PASSWORD = "ADMIN1";

    @NotNull
    private static final String BAD_TOKEN = UUID.randomUUID().toString();

    @Test
    public void login() {
        Assert.assertThrows(Exception.class, ()->endpoint.login(new UserLoginRequest()));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN_PASSWORD, ADMIN_LOGIN_PASSWORD);
        @NotNull final UserLoginResponse response = endpoint.login(loginRequest);
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getToken());
        Assert.assertTrue(response.getSuccess());
        Assert.assertThrows(
                Exception.class,
                ()->endpoint.login(new UserLoginRequest(NEW_USER_LOGIN_PASSWORD, NEW_USER_LOGIN_PASSWORD))
        );
        Assert.assertThrows(
                Exception.class,
                ()->endpoint.login(new UserLoginRequest(ADMIN_LOGIN_PASSWORD, NEW_USER_LOGIN_PASSWORD))
        );
        Assert.assertThrows(Exception.class, ()->endpoint.login(new UserLoginRequest(null, ADMIN_LOGIN_PASSWORD)));
        Assert.assertThrows(Exception.class, ()->endpoint.login(new UserLoginRequest(ADMIN_LOGIN_PASSWORD, null)));
        Assert.assertThrows(Exception.class, ()->endpoint.login(new UserLoginRequest(null, null)));
    }

    @Test
    public void logout() {
        Assert.assertThrows(Exception.class, ()->endpoint.logout(new UserLogoutRequest()));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN_PASSWORD, ADMIN_LOGIN_PASSWORD);
        @NotNull final UserLoginResponse response = endpoint.login(loginRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(response.getToken());
        @NotNull final UserLogoutResponse logoutResponse = endpoint.logout(logoutRequest);
        Assert.assertNotNull(logoutResponse);
        Assert.assertTrue(logoutResponse.getSuccess());
        Assert.assertThrows(Exception.class, ()->endpoint.logout(new UserLogoutRequest(BAD_TOKEN)));
    }

    @Test
    public void profile() {
        Assert.assertThrows(Exception.class, ()->endpoint.profile(new UserProfileRequest()));
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest(ADMIN_LOGIN_PASSWORD, ADMIN_LOGIN_PASSWORD);
        @NotNull final UserLoginResponse loginResponse = endpoint.login(loginRequest);
        @NotNull final UserProfileRequest profileRequest = new UserProfileRequest(loginResponse.getToken());
        @NotNull final UserProfileResponse profileResponse = endpoint.profile(profileRequest);
        Assert.assertNotNull(profileResponse);
        Assert.assertNotNull(profileRequest.getToken());
        Assert.assertNotNull(profileResponse.getUser());
        Assert.assertThrows(Exception.class, ()->endpoint.profile(new UserProfileRequest(BAD_TOKEN)));
    }

}
